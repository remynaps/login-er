from os import environ
from os.path import join, dirname

config = {
    'STAPELTJE_CLIENT': {
        'consumer_key': environ['STAPELTJE_CLIENT_ID'],
        'consumer_secret': environ['STAPELTJE_CLIENT_SECRET'],
        'STAPELTJE_CALLBACK': environ['STAPELTJE_CALLBACK'],
    },
}