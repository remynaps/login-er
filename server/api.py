import datetime
import secrets
from flask import Flask, render_template, redirect
from flask import Blueprint, render_template, session, request, redirect, abort, url_for, current_app, Response
from config import config
from urllib.parse import quote, urlencode
from random_string import generate_random
from flask_oauthlib.client import OAuth
from flask_wtf import CSRFProtect
import jwt

app = Flask(__name__)
app.config.update(**config)
app.secret_key = 'super secret key'

auth = OAuth()
CSRFProtect(app)

# Create a new state string
def prepare_state():
    session['oauth_state'] = secrets.token_urlsafe()

def prepare_nonce():
    session['openid_nonce'] = secrets.token_urlsafe()

# Get the state from the session
def get_state():
    return session['oauth_state']

def get_nonce():
    return session['openid_nonce']

# Validate the state token for the current login flow
def validate_state():
    if 'oauth_state' not in session:
        abort(Response(render_template('login-timed-out.html'), 401))
    expected_state = session['oauth_state']
    if expected_state is None:
        abort(Response(render_template('login-timed-out.html'), 401))
    del session['oauth_state']
    if request.args.get('state') != expected_state:
        abort(Response(render_template('login-timed-out.html'), 401))

def validate_nonce(nonce):
    if 'openid_nonce' not in session:
        abort(Response(render_template('login-timed-out.html'), 401))
    expected_nonce = session['openid_nonce']
    if expected_nonce is None:
        abort(Response(render_template('login-timed-out.html'), 401))
    del session['openid_nonce']
    if nonce != expected_nonce:
        abort(Response(render_template('login-timed-out.html'), 401))

stapeltje = auth.remote_app(
    'stapeltje',
    base_url='https://id.stapeltje.nl/oauth/',
    request_token_url=None,
    access_token_method='POST',
    access_token_url='https://id.stapeltje.nl/oauth/token',
    authorize_url='https://id.stapeltje.nl/oauth/authorise',
    request_token_params={'scope': 'openid email'},
    app_key='STAPELTJE_CLIENT',
)

secure_url_for = lambda x: url_for(x, _external=True, _scheme='https' if not current_app.debug else 'http')

@app.route("/")
def home():
    return render_template('home.html')

# @app.route("/login")
# def login():
#     app_id = app.config['APP_ID']
#     app_secret = app.config['APP_SECRET']
#     redirect_url = app.config['REDIRECT_URL']
#     state = generate_random(10)
#     params = {'client_id': app_id, 'client_secret': app_secret, 'redirect_url': redirect_url,  'state': state}
#     return redirect(app.config['AUTH_URL'] + '?' + urlencode(params, quote_via=quote), code=302)

@app.route("/login")
def google_auth_start():
    prepare_state()
    prepare_nonce()
    return stapeltje.authorize(secure_url_for('stapeltje_auth_complete'), get_state(), nonce=get_nonce())


@app.route("/callback")
def stapeltje_auth_complete():
    validate_state()
    resp = stapeltje.authorized_response()
    if resp is None:
        return 'Failed.'
    stapeltje_token = (resp['access_token'],)
    token_info = jwt.decode(resp['id_token'], verify=False)
    app.logger.error('logged in: %s ', token_info)
    idp_user_id = token_info['sub']
    validate_nonce(token_info['nonce'])
    email = token_info['email']
    return render_template('loggedin.html',email=email)

# @app.route("/callback")
# def loggedin():
#     token = request.args.get('token')
#     if token:
#         return render_template('loggedin.html')
#     else:
#         return redirect('/')

@app.route("/logout")
def logout():
    return redirect('https://id.stapeltje.nl/auth' + '/logout')

@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('404.html'), 404
    